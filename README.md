# MovieIonic

App Mobile-Híbrida implementada em Ionic 3

Esta app apresenta um catálogo de filmes

** Este repositório faz parte dos conteúdos da disciplina de Sistemas Embebidos da [Universidade Lusófona](https://www.ulusofona.pt).**

---

## Instruções

A funcionalidade da App não está completa.

[Deverá fazer fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) deste repositório para um repositório com o nome: *username*.bitbucket.org e depois fazer checkout para o repositório local na máquina.
Após o Checkout para a máquina local necessita correr o comando **npm install** para instalar todas as dependências localmente.

Nesse repositório, deverá fazer as seguintes alterações:

* Corrigir erro na tabbar, que para a opção Watched abre a tab Wishlist
* Adicionar o filme [The Dark Knight](www.imdb.com/title/tt0468569/) ou outro à lista de filmes
* Completar o form de criação de um novo filme para recolher os campos 'Director', 'Stars' e 'Genre'
* Adicionar acção ao floating action button existente na lista de pesquisa de filmes. Este botão deve abrir o ecrã com o form para adição de um filme
* Terminar a implementação da função de pesquisa através de uma [search bar](https://ionicframework.com/docs/components/#searchbar)
* Implementar função que permite filtrar os filmes para apresentar apenas os marcados como vistos 'watched'
* Terminar a implementação das opções de adicionar à wishlist e marcar como watched a partir do slide de um elemento da lista no ecrã de pesquisa.
* Implementação a notificação de alteração do modelo de dados para atualização das listas de filmes watched e na wishlist
* Implementar validação no form para garantir que o campo runtime é numérico e tem obrigatoriamente 2 ou 3 digitos.
* Utilizar o componente [Ionic Slides](https://ionicframework.com/docs/components/#slides) para adicionar uma galeria com fotos de um filme, acessível através do ecrã de detalhe do filme
* Utilizar o [Ionic Storage](https://ionicframework.com/docs/storage/) para ter persistência de dados entre execuções, nomeadamente para guardar filmes criados através do form.
