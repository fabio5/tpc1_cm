import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { SearchPage } from '../pages/search/search';
import { WatchedPage } from '../pages/watched/watched';
import { WishlistPage } from '../pages/wishlist/wishlist';
import { MovieDetailPage } from '../pages/produto-detail/produto-detail';
import { AddMoviePage } from '../pages/add-produto/add-produto';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProdutoProvider } from '../providers/produto-provider';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    SearchPage,
    WatchedPage,
    WishlistPage,
    MovieDetailPage,
    AddMoviePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    SearchPage,
    WatchedPage,
    WishlistPage,
    MovieDetailPage,
    AddMoviePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProdutoProvider
  ]
})
export class AppModule {}
