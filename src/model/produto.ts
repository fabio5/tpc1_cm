
export class Movie {

  constructor(
    public title: string,
    public thumb: string,
    public price: number,
    // public validade: number;
    public sinopse: string,
    public marcaprod: string,
    public runtime: number,
    public stars: string,
    public genre: string,
    public watched: boolean,
    public wishlist: boolean) {
  }

}
