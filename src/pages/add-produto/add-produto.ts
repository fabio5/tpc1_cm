import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, NavParams, ViewController, ToastController} from 'ionic-angular';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Movie } from '../../model/produto';

@Component({
  selector: 'page-add-produto',
  templateUrl: 'add-produto.html',
})
export class AddMoviePage {

  movieForm: any;
  submitAttempt: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
    public moviesProvider: ProdutoProvider
  ) {

    // Tutorial with more complex FormBuilder and Validators at https://www.joshmorony.com/advanced-forms-validation-in-ionic-2/
    this.movieForm = formBuilder.group({
        title: ['', Validators.required],
        price: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{4}')])],
        thumb: ['', Validators.required],
        sinopse: ['', Validators.required],
        runtime: ['', Validators.required],
        marcaprod: [''],
        stars: [''],
        genre: ['']
    });
  }


  saveMovie() {
    // Flag used to avoid displaying errors on first form appearance
    this.submitAttempt = true;

    // Validate form
    if(!this.movieForm.valid){
      console.log("Error on Form!")
      this.showToastNotification("Error on Form!");
    }
    else {
      // Create movie object
      let newMovie = new Movie(
        this.movieForm.value.title,
        this.movieForm.value.thumb,
        this.movieForm.value.price,
        this.movieForm.value.sinopse,
        null,
        this.movieForm.value.runtime,
        null,
        null,
        false,
        false
      );

      this.moviesProvider.save(newMovie);
      this.moviesProvider.publishChange(newMovie);


      console.log("Movie '" + newMovie.title + "' Saved!")
      this.showToastNotification("Movie '" + newMovie.title + "' Saved!");
      
      this.viewCtrl.dismiss();
    }
  }


  showToastNotification(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
