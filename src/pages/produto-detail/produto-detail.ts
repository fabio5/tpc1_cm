import { Component } from '@angular/core';
import { NavParams, Events } from 'ionic-angular';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Movie } from '../../model/produto';

@Component({
  selector: 'page-produto-detail',
  templateUrl: 'produto-detail.html'
})
export class MovieDetailPage {

  movie: Movie;

  constructor(public navParams: NavParams, public events: Events, public moviesProvider: ProdutoProvider) {
    this.movie = this.navParams.get('selectedMovie');
    console.log("Showing detail for movie '" + this.movie.title + "'");
  }

  toggleChanged() {
    // TODO: Make sure change is published so that subcribers can reload data.
    // Current subscribers are Watched and Wishlist Pages.
    // See Events https://ionicframework.com/docs/api/util/Events/
  }

}
