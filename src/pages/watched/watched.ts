import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { MovieDetailPage } from '../produto-detail/produto-detail';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Movie } from '../../model/produto';

@Component({
  selector: 'page-watched',
  templateUrl: 'watched.html'
})
export class WatchedPage {

  movies: Movie[];

  constructor(public navCtrl: NavController, public events: Events, public moviesProvider: ProdutoProvider) {

    this.filterMovieData();

    this.events.subscribe('movie-data-changed', (movie) => {
      this.filterMovieData();
    });
  }

  filterMovieData() {
    
  }

  openMovieDetail(movie: any) {
    this.navCtrl.push(MovieDetailPage, { selectedMovie: movie });
  }

}
